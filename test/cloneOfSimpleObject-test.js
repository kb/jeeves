const should = require("chai").should()

function simpleObjectCloneOf(obj) {
  let rtn = {}
  for (let [k, v] of Object.entries(obj)) {
    rtn[k] = v
  }
  return rtn
}

describe("Function: simpleObjectCloneOf", () => {
  it.only("Breaks reference to simple object.", () => {
    let o = {a: 1, b: 2}
    let b = o
    o.a = 3
    b.a.should.eql(3)
    b = simpleObjectCloneOf(o)
    o.a = 1
    b.a.should.eql(3)
  })
})
