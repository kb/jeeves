jeeves - test scaffold
**********************

Thank you Tim :)

.. code-block:: bash

  cd ~/DevFolder/
  mkdir jeeves
  cd jeeves
  npm init -y
  npm i mocha chai --save-dev
  mkdir test
  touch test/it-never-fails-test.js
  touch test/cloneOfSimpleObject-test.js

Copy the following into test/it-never-fails-test.js

.. code-block:: javascript

  const should = require("chai").should()

  describe("NeverFails", () => {
    it.skip("really really never fails", () => {
      true.should.be.ok
    })
  })

Copy your test code into: e.g. ``test/cloneOfSimpleObject-test.js``

Open ``package.json`` and replace ``scripts`` entry with:

.. code-block:: javascript

  "scripts": {
    "test": "mocha"
  },

Run::

  npm test

Issues
======

That works very nicely... although it doesn't seem to run
``test/it-never-fails-test.js``

  Notice that the test has ``it.skip`` and the clone test has ``it.only``
  More often you won't need never fails to run... only to check your test
  environment is behaving. Other time you *only* want one test to run ...
  especially if you console log... because you want to reduce test feedback
  noise.
